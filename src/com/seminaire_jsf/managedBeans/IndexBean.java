package com.seminaire_jsf.managedBeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.seminaire_jsf.dao.TodoDao;
import com.seminaire_jsf.entities.Todo;

@Named
@ViewScoped
public class IndexBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<Todo> todos;
	private TodoDao dao;

	@PostConstruct
	public void init() {
		System.out.println("IndexBean constructed!");
		dao = new TodoDao();
		todos = dao.findAll();
	}

	@PreDestroy
	public void teardown() {
		System.out.println("IndexBean destroyed!");
	}

	public String create() {
		return "create.xhtml?faces-redirect=true";
	}

	public String view(Integer id) {
		return "view.xhtml?faces-redirect=true&id=" + id;
	}

	public List<Todo> getTodos() {
		return todos;
	}

	public void setTodos(List<Todo> todos) {
		this.todos = todos;
	}
}
