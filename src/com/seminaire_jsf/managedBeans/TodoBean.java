package com.seminaire_jsf.managedBeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.seminaire_jsf.dao.TodoDao;
import com.seminaire_jsf.entities.Todo;

@Named
@ViewScoped
public class TodoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Todo todo;
	private TodoDao dao;

	@PostConstruct
	public void init() {
		dao = new TodoDao();
	}

	public void retrieve() {
		if (null == id) {
			todo = new Todo();
		} else {
			todo = dao.findById(id);
		}
	}

	public String save() {
		if (null == id) {
			todo = dao.create(todo);
		} else {
			todo = dao.update(todo);
		}
		return "view.xhtml?faces-redirect=true&id=" + todo.getId();
	}

	public String delete() {
		dao.delete(todo);
		return "index.xhtml?faces-redirect=true";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Todo getTodo() {
		return todo;
	}

	public void setTodo(Todo todo) {
		this.todo = todo;
	}
}
