package com.seminaire_jsf;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.seminaire_jsf.dao.TodoDao;
import com.seminaire_jsf.entities.Todo;

@WebListener
public class Initializer implements ServletContextListener {

	private static TodoDao dao;

	public Initializer() {
		dao = new TodoDao();
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		dao.create(new Todo("Titre 1", "Description 1",
				Date.from(LocalDateTime.now().minusDays(2).atZone(ZoneId.systemDefault()).toInstant())));
		dao.create(new Todo("Titre 2", "Description 2",
				Date.from(LocalDateTime.now().plusDays(1).atZone(ZoneId.systemDefault()).toInstant())));
		dao.create(new Todo("Titre 3", "Description 3",
				Date.from(LocalDateTime.now().plusDays(3).atZone(ZoneId.systemDefault()).toInstant())));
		dao.create(new Todo("Titre 4", "Description 4",
				Date.from(LocalDateTime.now().minusDays(4).atZone(ZoneId.systemDefault()).toInstant())));
		dao.create(new Todo("Titre 5", "Description 5", null));
		dao.create(new Todo("Titre 6", "Description 6",
				Date.from(LocalDateTime.now().plusWeeks(1).atZone(ZoneId.systemDefault()).toInstant())));
	}

}
