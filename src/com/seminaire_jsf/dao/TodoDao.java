package com.seminaire_jsf.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.seminaire_jsf.entities.Todo;

public class TodoDao {

	EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
	EntityTransaction transaction;

	public Todo create(Todo todo) {
		transaction = entityManager.getTransaction();
		transaction.begin();
		try {
			entityManager.persist(todo);
			return todo;
		} catch (Exception e) {
			System.err.println("Error creating todo: " + e.getMessage());
			transaction.rollback();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
		return null;
	}

	public Todo update(Todo todo) {
		transaction = entityManager.getTransaction();
		transaction.begin();
		try {
			entityManager.merge(todo);
			return todo;
		} catch (Exception e) {
			System.err.println("Error updating todo: " + e.getMessage());
			transaction.rollback();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
		return null;
	}

	public Todo findById(Integer id) {
		transaction = entityManager.getTransaction();
		transaction.begin();
		try {
			return entityManager.find(Todo.class, id);
		} catch (Exception e) {
			System.err.println("Error finding todo by id: " + e.getMessage());
			transaction.rollback();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
		return null;
	}

	public void delete(Todo todo) {
		transaction = entityManager.getTransaction();
		transaction.begin();
		try {
			entityManager.remove(todo);
		} catch (Exception e) {
			System.err.println("Error deleting todo: " + e.getMessage());
			transaction.rollback();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
	}

	public List<Todo> findAll() {
		transaction = entityManager.getTransaction();
		transaction.begin();
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Todo> criteriaQuery = criteriaBuilder.createQuery(Todo.class);
			Root<Todo> rootEntry = criteriaQuery.from(Todo.class);
			CriteriaQuery<Todo> all = criteriaQuery.select(rootEntry);
			return entityManager.createQuery(all).getResultList();
		} catch (Exception e) {
			System.err.println("Error finding all users: " + e.getMessage());
			transaction.rollback();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
		return new ArrayList<Todo>();
	}
}
