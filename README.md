# Plan de présentation

1. Présentation du sujet
2. Démo hello world
3. Introduction aux concepts de JSF
4. Démo application "todo"

---
# Démo hello world

Dans cette partie, le présentateur explique comment créer un projet JSF très simple. Il s'agit de montrer comment la configuration initiale est similaire à d'autres technologies utilisées dans le cadre de la formation spécifique.

## Prérequis

- Eclipse
- [Serveur Tomcat](https://tomee.apache.org/download-ng.html) prêt à être utilisé

## Marche à suivre

1. À partir d'Eclipse, créez un nouveau projet de type Dynamic Web Project. Cochez l'option "Generate web.xml" situé à la dernière page.
2. Allez dans les propriétés du projet (clic-droit sur le projet -> "Properties"), puis dans "Project Facets" et sélectionnez JavaServerFaces et JAX-RS.
3. Convertissez le projet en projet Maven (clic-droit sur le projet -> "Configure"  -> "Convert to Maven Project")
4. Créez un fichier "demo.xhtml" dans le dossier "WebContent"
5. Créez une classe "HelloWorld.java" dans le dossier "src" (lui-même situé dans le dossier Java Resources)
6. Ajouter le projet sur le serveur(clic-droit sur le serveur -> "Add and Remove..." -> le nom de votre projet) et démarrez le serveur
7. Vous pouvez maintenant accéder à votre page avec "http://localhost:8080/demo_helloworld/faces/demo.xhtml" (en assumant que votre serveur s'exécute sur le port 8080 et que votre projet se nomme "demo_helloworld")

### Extraits de code

demo.xhtml
```
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:ui="http://java.sun.com/jsf/facelets">
	
	<h:head>
		<title>Exemple hello world</title>
	</h:head>
	
	<h:body>
		<h:outputText value="#{helloWorld.greeting}" />
	</h:body>
</html>
```

HelloWorld.java
```
package demo_helloworld;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class HelloWorld {

	private String greeting = "Hello world!";

	public String getGreeting() {
		return greeting;
	}

	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}
}
```

---
# Introduction aux concepts de JSF

Cette partie utilisera un document powerpoint(inclus dans ce répertoire git) pour présenter certains concepts clés de JSF. Pour simplifier le processus, l'information donnée par le présentateur est également disponible ci-dessous.

## Qu'est-ce que JSF?

JavaServer Faces est un framework web qui facilite la création d'interfaces utilisateurs pour des applications serveurs. La force de JSF réside dans ces interfaces utilisateurs en permettant l'utilisation de gabarits réutilisables, en facilitant l'échange de données entre ces gabarits et en gérant l'état du client à travers différentes requêtes faite au serveur.

## Cycle de vie

Le cycle de vie d'une application JSF est composé de 6 phases : restore view, apply request value, process validation, update model values, invoke application, render response. Ces 6 phases sont traversées dès qu'une action est faite sur une page client (lien, bouton, etc).

- Restore View
- Apply Request
- Process Validation
- Update Model Values
- Invoke Application
- Render response

## Managed beans et scopes

Agit comme le model d'un composant. L'annotation @ManagedBean ou @Named permet d'appeler les variables et méthodes de cette classe dans les fichiers xhtml. Ces managed beans ont tous un scope qui définit la durée de vie de la classe. 
1. @RequestScoped : scope par défaut. Le bean existe aussi longtemps que le cycle de vie de la requête HTTP subsiste.
2. @NoneScoped : Le bean n'existe que pour le temps d'une évaluation EL.
3. @ViewScoped : Le bean existe tant que l'utilisateur interagit avec la même vue JSF dans le même navigateur.
4. @SessionScoped : Le bean est créé suite à la première requête HTTP et existe tant que la session HTTP est valide.
5. @ApplicationScoped : Comme le scope précédent, le bean est créé suite à la première requête HTTP qui l'appelle, puis existe tant que l'application est en cours.
6. @CustomScoped : Permet de configurer une durée de vie personnalisée dans un custom map(élaborer)

## Templating

La force de JSF réside dans la facilité d'utilisation des gabarits. Il existe quelques tags qui permette de gérer ces gabarits.
- <ui:define>: Définit du contenu qui sera inséré dans un template ayant un tag ui:insert correspondant.
- <ui:insert>: Utilisé dans un gabarit, ce tag définit du contenu qui sera remplacé par un gabarit.
- <ui:include>: Permet d'inclure le contenu d'un autre fichier xhtml.
- <ui:composition>: Si ce tag est utilisé avec l'attribut template, le gabarit correspondant est chargé avec les enfants du tag <ui:composition>, si on souhaite changer les gabarits.

## Intégration avec Spring

Spring fournit une classe spéciale nommée DelegatingVariableResolver. Les étapes suivantes sont nécessaire pour intégrer la fonctionalité de Spring Dependency Injection à JSF:
- Ajouter une entrée variable-resolver à faces-config.xml qui pointe vers la classe Spring
- Ajouter un ContextLoaderListener et un RequestContextListener à web.xml
- Définir les beans qui seront utilisés comme dépendance dans les managed bean dans applicationContext.xml
- Ajouter la dépendance dans la configuration du managed bean dans faces-config.xml
Et voilà! Le managed bean peut maintenant utiliser une dépendance gérée par Spring.

## Navigation

JSF offre un mécanisme de navigation implicite qui permet de naviguer entre différentes pages en ne faisant qu'appeler le nom de la page. Cette navigation peut être effectuée à partir d'une page JSF :
```
<h:commandButton action = "page2" value = "Page2" />
```
ou à partir d'un managed bean, en assumant que la méthode retourne une string qui représente le nom de la page :
```
<h:commandButton action = "#{bean.action}" value = "Page2" />
```

## Event Handling

Dès qu'un utilisateur clique sur un bouton, un lien ou même change la valeur d'un champ texte, le UI Component correspondant trigger un event. Pour pouvoir gérer ces évènements, il est nécessaire d'avoir une méthode dans le backing bean. Quelques évènements qui peuvent être détectés par JSF:
- valueChangeListener: Cet event est lancé dès que la valeur du champ est changé
- actionListener: Cet event est lancé au clic d'un bouton ou d'un lien
- application events: Ces events sont lancé durant le cycle de vie JSF. Parmis ces évènements, on retrouve par exemple PostConstructApplicationEvent, PreDestroyApplicationEvent et PreRenderViewEvent.

## Internationalisation

Il est très facile d'internationaliser une application JSF. Il suffit de créer un fichier properties (ayant comme format file_locale.properties, par exemple index_en.properties) et d'indiquer dans le fichier faces-config.xml quels locale sont supportés et comment accéder au fichier.

## Expression Language

JSF fournit un expression language qui permet de:
- Faire référence à des attributs ou méthodes d'un managed bean
- Faciliter l'accès aux éléments d'une collection
- Facilite l'accès aux objets prédéfinit(tel que request) – à voir
- Effectuer des opérations arithmétiques,  logiques et relationnelles
- Convertir automatiquement les types des données
- Éviter les NPE

---
# Démo application TODO

Suite à la partie théorique, le présentateur effectuera un survol d'une application TODO pour montrer l'application des concepts mentionnés plus haut. Si le temps le permet, le présentateur implantera certaines fonctionalités additionnelles basées sur les concepts présentés.

---
# Sources et références

1. [Tutorialspoint](https://www.tutorialspoint.com/jsf)
2. [mkyong (templating)](https://www.mkyong.com/jsf2/jsf-2-templating-with-facelets-example/)
3. [Expression Language (documentation oracle)](https://docs.oracle.com/javaee/7/tutorial)